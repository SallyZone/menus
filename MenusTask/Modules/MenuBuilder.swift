//
//  MenuBuilder.swift
//  MenusTask
//
//  Created by Sally Ahmed on 12/9/17.
//  Copyright (c) 2017 Sally Ahmed. All rights reserved.
//

import UIKit

struct MenuBuilder {

    static func viewController() -> BaseViewController {
        let viewModel = MenuViewModel()
        let router = MenuRouter()
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController: MenuViewController = mainStoryboard.instantiateViewController(withIdentifier: "MenuView") as! MenuViewController
        viewController.setUp(withViewModel: viewModel, router: router)
    
        router.viewController = viewController

        return viewController
    }
}
