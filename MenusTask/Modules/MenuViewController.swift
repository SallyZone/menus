//
//  MenuViewController.swift
//  MenusTask
//
//  Created by Sally Ahmed on 12/9/17.
//  Copyright (c) 2017 Sally Ahmed. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class MenuViewController: BaseViewController ,UITableViewDelegate {
    fileprivate var viewModel: MenuViewModel!
    fileprivate var router: MenuRouter!
    fileprivate let disposeBag = DisposeBag()
    fileprivate var items = Variable<[SectionModel<String, Item>]>([])
    fileprivate var menu :Menu!
    fileprivate var tempMenu :Menu!

    @IBOutlet weak var placeholderLbl: UILabel!
    @IBOutlet weak var menuTbl: UITableView!
    func setUp(withViewModel viewModel: MenuViewModel, router: MenuRouter) {
        self.viewModel = viewModel
        self.router = router
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.menuTbl.isHidden = true
        self.placeholderLbl.text = "Loading..."
        self.title = "Menu"
        setupRx()
    }
    let dataSource = RxTableViewSectionedReloadDataSource<SectionModel<String, Item>>(
        configureCell: { (_, tv, indexPath, element) in
            let cell:MenuTableViewCell = tv.dequeueReusableCell(withIdentifier: "MenuCell") as! MenuTableViewCell
            cell.item = element
            return cell
    },
        titleForHeaderInSection: { dataSource, sectionIndex in
            return dataSource[sectionIndex].model
    }
    )

    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
        let view:UIView = UIView(frame:CGRect(x: 8, y: 8, width: tableView.frame.size.width - 16, height: 40))
        view.backgroundColor = UIColor.lightGray
        let titleLbl :UILabel = UILabel(frame:CGRect(x: 8, y: 0, width: view.frame.size.width, height: view.frame.size.height))
        titleLbl.text = dataSource[section].model
        
        let button:UIButton = UIButton(frame: view.frame)
        view.addSubview(titleLbl)
        view.addSubview(button)
        button.tag = section;
        button.addTarget(self, action: #selector(expandCollapse(_:)), for: .touchUpInside)
        return view
    }
    // for set value empty or real value depend on collapse/ expand
    @objc func expandCollapse(_ sender: UIButton){

        let isEmpty:Bool = self.items.value[sender.tag].items == [] ? true : false
       // animte to selected section
        if(isEmpty){
        self.items.value[sender.tag].items = self.menu.categories[sender.tag].items
        self.menuTbl.selectRow(at: IndexPath(row: 0, section: sender.tag), animated: true
        , scrollPosition: UITableViewScrollPosition.top)
        self.menuTbl.scrollToRow(at: IndexPath(row: 0, section: sender.tag), at: UITableViewScrollPosition.top, animated: true )
        }
        else{
            self.items.value[sender.tag].items  = []
        }
    }
}

// MARK: Setup
private extension MenuViewController {

    func setupRx() {
        
        self.viewModel.subject.subscribe(onNext: { (menu) in
            self.menu = menu
            self.tempMenu = menu
            self.items.value =
                menu.categories.map({ (obj) -> SectionModel<String, Item> in
                    SectionModel(model: obj.name , items:[])
                })
          
            
            self.items.asObservable()
                .bind(to: self.menuTbl.rx.items(dataSource: self.dataSource))
                .disposed(by: self.disposeBag)
            self.menuTbl.isHidden = false
            self.menuTbl.rx
                .setDelegate(self)
                .disposed(by: self.disposeBag)
            
        }, onError: { (error) in
            self.placeholderLbl.text = error.localizedDescription

        }, onCompleted: {
            self.placeholderLbl.text = ""

        }).disposed(by: disposeBag)
        
       
    
    }
    
    
}
