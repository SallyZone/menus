//
//  MenuViewModel.swift
//  MenusTask
//
//  Created by Sally Ahmed on 12/9/17.
//  Copyright (c) 2017 Sally Ahmed. All rights reserved.
//

import RxSwift
import RxAlamofire

class MenuViewModel {
    let disposeBag = DisposeBag()
    let menuURL = "http://elmenus.getsandbox.com/menu"
    let subject = PublishSubject<Menu>()

    init() {
        setupRx()
    }
}

// MARK: Setup
private extension MenuViewModel {

    func setupRx() {
        RxAlamofire.requestJSON(.get, menuURL,headers:["content-Type":"application/json"])
            .debug()
            .subscribe(onNext: { [weak self] (r, json) in
                if let jsonD:[String :Any] = json as? [String : Any] {
                    let menu = Menu(fromDictionary: jsonD)
                    self?.subject.onNext(menu)
                }
                
                },onError:{ [weak self] (error) in
                    self?.subject.onError(error)
            })
            .disposed(by: disposeBag)
    }
}
