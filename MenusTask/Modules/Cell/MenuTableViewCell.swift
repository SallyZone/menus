//
//  MenuTableViewCell.swift
//  MenusTask
//
//  Created by Sally Ahmed on 12/9/17.
//  Copyright © 2017 Sally Ahmed. All rights reserved.
//

import UIKit

class MenuTableViewCell: BaseTableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var likeBtn: UIButton!
    
    var item:Item!{
        didSet{
            self.titleLbl.text = item.name
            self.descriptionLbl.text = item.descriptionField
            self.likeBtn .setTitle("Like", for: UIControlState.normal)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        likeBtn.backgroundColor = .clear
        likeBtn.layer.cornerRadius = 5
        likeBtn.layer.borderWidth = 1
        likeBtn.layer.borderColor = UIColor.black.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
