//
//  BaseTableViewCell.swift
//  MenusTask
//
//  Created by Sally Ahmed on 12/9/17.
//  Copyright © 2017 Sally Ahmed. All rights reserved.
//

import UIKit

class BaseTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
